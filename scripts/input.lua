local M = {}

M.tapThreshold = 0.2    -- time in ms
M.dragThreshold = 5     -- radius in px

function M.tap(actionTouch, state)
    if actionTouch then
        if #actionTouch == 1 then
            if actionTouch[1].pressed then
                state.lastTouchTime = socket.gettime()
                state.isPossibleTap = true

                state.isTap = false
                state.tapPos = nil
                return
            end

            if state.isPossibleTap then
                if (socket.gettime() - state.lastTouchTime) >= M.tapThreshold then
                    state.isPossibleTap = false

                    state.isTap = false
                    state.tapPos = nil
                    return
                end

                if actionTouch[1].released then
                    state.isPossibleTap = false
                    
                    state.isTap = true
                    state.tapPos = vmath.vector3(actionTouch[1].x, actionTouch[1].y, 0)
                    return
                end
            end
        else
            state.isPossibleTap = false
        end
    end

    state.isTap = false
    state.tapPos = nil
end

function M.drag(actionTouch, state)
    if actionTouch then
        if #actionTouch == 1 then
            --
        end
    end
end

function M.zoom(actionTouch, state) -- aka pinch
    if actionTouch then
        if #actionTouch == 2 then
            local pos1 = vmath.vector3(actionTouch[1].x, actionTouch[1].y, 0)
            local pos2 = vmath.vector3(actionTouch[2].x, actionTouch[2].y, 0)
            local diff = pos2 - pos1
            local touchDistance = vmath.length(diff)
            local centerTouchPos = pos1 + vmath.normalize(diff) * touchDistance * 0.5

            local isFirstFrame_ = actionTouch[2].pressed
            if isFirstFrame_ then
                state.firstTouchCenterPos = centerTouchPos
                state.firstTouchDistance = touchDistance
            end

            local zoom = {
                ratio = touchDistance / state.firstTouchDistance,
                centerPosDifference = centerTouchPos - state.firstTouchCenterPos,
                isFirstFrame = isFirstFrame_
            }

            state.isZoom = true
            state.zoom = zoom
            return
        end
    end

    state.isZoom = false
    state.zoom = nil
end

return M