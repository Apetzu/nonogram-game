local camera = require "orthographic.camera"
local input = require "scripts.input"

local spriteSize = 256   -- = cell size
local halfSpriteSize = spriteSize * 0.5
local nonogramMarginPercent = 0.2
local labelMargin = vmath.vector3(200, 150, 0)

local cellStates = {
    empty = 1,
    filled = 2,
    crossed = 3
}

local function getPicPixel(self, x, y)
    local picPos = (self.level.size.y - 1 - y) * self.level.size.x + x
    local puzzleInt = self.level.pic[math.floor(picPos / 32) + 1]
    local bitPos = bit.lshift(1, picPos % 32)
    return bit.band(puzzleInt, bitPos) ~= 0
end

local function createLabels(self)
    self.labels = {}
    self.labels.row = {}
    self.labels.column = {}
    
    -- go through rows and columns (both sides)
    for side = 1, 2, 1 do
        local pos = (side == 1) and vmath.vector3(-labelMargin.x, 0.0, 0.0) or vmath.vector3(0.0, self.actualGridSize.y + labelMargin.y, 0.0)
        -- i and j = row and column, or column and row
        for i = 0, (side == 1 and self.level.size.y or self.level.size.x) - 1, 1 do
            -- create labels
            local posChange = i * spriteSize + halfSpriteSize
            local newGo
            if side == 1 then
                pos.y = posChange
                newGo = factory.create("clueLabels#rowLabelFactory", pos)
                self.labels.row[i] = newGo
            else
                pos.x = posChange
                newGo = factory.create("clueLabels#columnLabelFactory", pos)
                self.labels.column[i] = newGo
            end
            go.set_parent(newGo, go.get_id("clueLabels"))

            -- create clue texts and set it to the labels
            local labelText = ""
            local lastClueNumber = 0
            for j = (side == 1) and 0 or self.level.size.y - 1, (side == 1) and self.level.size.x - 1 or 0, (side == 1) and 1 or -1 do
                local isFilled = false
                if side == 1 then
                    isFilled = getPicPixel(self, j, i)
                else
                    isFilled = getPicPixel(self, i, j)
                end

                if isFilled then
                    lastClueNumber = lastClueNumber + 1
                else
                    if lastClueNumber > 0 then
                        labelText = labelText .. lastClueNumber .. (side == 1 and "  " or "\n")
                        lastClueNumber = 0
                    end
                end
            end
            if lastClueNumber > 0 then
                labelText = labelText .. lastClueNumber .. (side == 1 and "  " or "\n")
            end
            label.set_text(msg.url(nil, newGo, "label"), labelText)
        end
    end
end

local function createNonogram(self, level)
    self.level = level
    self.cells = {}

    -- it's easier to use window size as a vector
    local winWidth, winHeight = camera.get_window_size()
    self.winSize = vmath.vector3(winWidth, winHeight, 0)

    self.actualGridSize = self.level.size * spriteSize

    -- the entire nonogram will be first spawned in 100% scale (in actual size) and then downscaled and centered according to margins and window size in on_message!

    -- create cells in the grid
    for y = 0, self.level.size.y - 1, 1 do
        self.cells[y] = {}
        for x = 0, self.level.size.x - 1, 1 do
            local pos = vmath.vector3(x * spriteSize, y * spriteSize, 0.0)
            local newGo = factory.create("grid#cellFactory", pos)
            go.set_parent(newGo, go.get_id("grid"))
            self.cells[y][x] = { id = newGo, state = cellStates.empty }
        end
    end

    -- create lables (numbers/clues) around the grid
    createLabels(self)
    msg.post("#", "labelUpdate")
end

local function changeCell(self, x, y, state, flipOn)
    local cell = self.cells[y][x]

    if not state then
        if cell.state == cellStates.crossed then
            cell.state = cellStates.empty
        else
            cell.state = cell.state + 1
        end
    else
        if flipOn and cell.state == state then
            cell.state = cellStates.empty
        else
            cell.state = state
        end
    end

    sprite.play_flipbook(msg.url(nil, cell.id, "cellSprite"), hash("cell" .. cell.state))
end

local function showSolution(self)
    for y = 0, self.level.size.y - 1, 1 do
        for x = 0, self.level.size.x - 1, 1 do
            local picPixel = getPicPixel(self, x, y)
            changeCell(self, x, y, picPixel and cellStates.filled or cellStates.empty)
        end
    end
end

local function checkNonogram(self)
    for y = 0, self.level.size.y - 1, 1 do
        for x = 0, self.level.size.x - 1, 1 do
            local picPixelState = getPicPixel(self, x, y)
            local cellState = self.cells[y][x].state == cellStates.filled
            if cellState ~= picPixelState then
                return false
            end
        end
    end

    return true
end

function init(self)
    msg.post(".", "acquire_input_focus")

    self.inputState = {}
end

function on_message(self, message_id, message, sender)
    if message_id == hash("createNonogram") then
        createNonogram(self, message)
    elseif message_id == hash("labelUpdate") then
        -- get label max sizes
        local maxLabelSize = vmath.vector3(0, 0, 0)
        for row = 0, self.level.size.y - 1, 1 do
            local rowLabelSize = label.get_text_metrics(msg.url(nil, self.labels.row[row], "label")).width
            if rowLabelSize > maxLabelSize.x then
                maxLabelSize.x = rowLabelSize
            end
        end
        for column = 0, self.level.size.x - 1, 1 do
            local columnLabelSize = label.get_text_metrics(msg.url(nil, self.labels.column[column], "label")).height
            if columnLabelSize > maxLabelSize.y then
                maxLabelSize.y = columnLabelSize
            end
        end
        maxLabelSize = maxLabelSize + labelMargin

        -- take account grid and labels sizes and move the entire nonogram middle of the screen
        local halfActualGridSize = self.actualGridSize * 0.5
        local actualCenterOffset = vmath.vector3(maxLabelSize.x * 0.5 - halfActualGridSize.x, -maxLabelSize.y * 0.5 - halfActualGridSize.y, 0)
        go.set_position(actualCenterOffset, "grid")
        go.set_position(actualCenterOffset, "clueLabels")

        -- take account margin and window size, and scale the nonogram
        local nonogramMargin = vmath.vector3(self.winSize.x * nonogramMarginPercent, self.winSize.y * nonogramMarginPercent * (self.winSize.x / self.winSize.y), 1)
        local actualNonogramSize = self.actualGridSize + maxLabelSize
        local screenSize = self.winSize - nonogramMargin
        local nonogramScale = math.min(screenSize.x / actualNonogramSize.x, screenSize.y / actualNonogramSize.y)
        go.set_scale(nonogramScale)

        -- variables for on_input
        self.gridBottomLeft = actualCenterOffset * nonogramScale
        self.gridTopRight = (actualCenterOffset + self.actualGridSize) * nonogramScale
        self.cellSize = spriteSize * nonogramScale
        self.gridUsable = true
    elseif message_id == hash("changeFillMode") then
        self.fillMode = message.fillMode
    end
end

function on_input(self, action_id, action)
    if self.gridUsable then
        input.tap(action.touch, self.inputState)

        if self.inputState.isTap then
            local worldCoord = camera.screen_to_world(go.get_id("/camera"), self.inputState.tapPos)

            if worldCoord.x > self.gridBottomLeft.x and worldCoord.x < self.gridTopRight.x then
                if worldCoord.y > self.gridBottomLeft.y and worldCoord.y < self.gridTopRight.y then
                    local x = math.floor((worldCoord.x - self.gridBottomLeft.x) / self.cellSize)
                    local y = math.floor((worldCoord.y - self.gridBottomLeft.y) / self.cellSize)
                    changeCell(self, x, y, self.fillMode, true)

                    if checkNonogram(self) then
                        showSolution(self)
                        msg.post(".", "release_input_focus")
                        msg.post("/camera#cameraMovement", "resetCamera")
                        msg.post("/camera#cameraMovement", "disableMovement")
                        msg.post("/ingameGUI#ingame", "showVictoryBox")
                    end
                end
            end
        end
    end
end