import sys
from textwrap import wrap

decs = []
if len(sys.argv) > 1:
    for byte in wrap(sys.argv[1], 32):
        decs.append(int(byte[::-1], 2))

print(decs)